package com.pivotal.certification.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleConfig {

    @Bean
    public SimpleBean simpleBeanImpl() {
        return new SimpleBeanImpl();
    }

    @Bean SimpleBean testBean() {
        return simpleBeanImpl();
    }

}

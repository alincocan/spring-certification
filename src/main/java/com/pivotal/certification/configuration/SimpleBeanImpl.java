package com.pivotal.certification.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleBeanImpl implements SimpleBean {

    Logger logger = LoggerFactory.getLogger(SimpleBean.class);

    public SimpleBeanImpl() {
        logger.info("[Simple bean impl instantiation]");
    }

    @Override public String toString() {
        return "SimpleBeanImpl{ code: " + hashCode() + "}";
    }
}

package com.pivotal.certification.beans;

public class Moto extends Vehicle {

    public Moto() {
        super("BV 95 TEO");
    }

    @Override public int getWheelsNr() {
        return 2;
    }
}

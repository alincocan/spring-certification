package com.pivotal.certification.beans;

public abstract class Vehicle {

    private String registrationNo;

    public Vehicle(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public abstract int getWheelsNr();

    public String getRegistrationNo() {
        return registrationNo;
    }
}

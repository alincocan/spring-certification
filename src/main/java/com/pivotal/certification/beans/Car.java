package com.pivotal.certification.beans;

public class Car extends Vehicle {

    public Car() {
        super("BV 95 AIC");
    }

    @Override
    public int getWheelsNr() {
        return 4;
    }
}



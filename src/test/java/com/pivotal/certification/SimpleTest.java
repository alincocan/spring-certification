package com.pivotal.certification;

import com.pivotal.certification.configuration.SimpleConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class SimpleTest {

    @Test
    public void tst() {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SimpleConfig.class);

        Arrays.asList(applicationContext.getBeanDefinitionNames())
            .forEach(System.out::println);

    }


}
